---
title: Postface
---
Poema "Bagëtia e bujqësia" u botua më 1886.Eshte një nga veprat më të bukura e më të frymëzuara jo vetëm të Naimit, por edhe të krejt letërsisë së Rilindjes. Askush para Naimit nuk i kishte kënduar dashurisë e mallit për atdhe, krenarisë kombëtare dhe bukurisë së natyrës shqiptare me një pasion aq të zjarrtë e me një gjuhë poetike aq të ëmbël e të bukur. Me këtë poemë Naimi krijoi poezinë e madhe të Atdheut e të natyrës shqiptare. Kjo vepër jo vetëm u bë e dashur për bashkatdhetarët, por krijoi një traditë të pasur edhe për poetët që erdhën më pas. Siç e tregon edhe titulli, poema përbëhet nga dy pjesë. Në pjesën e parë flitet për punët e blegtorisë, për jetën dhe punën e bariut, kurse në pjesën e dytë për punët e bujkut. Megjithatë, kjo s'është aspak një vepër për këto fusha të jetës. Prej tyre poeti vetëm sa merr pikënisje për të shprehur ndjenjat e flakta patriotike. Në të vërtetë, vepra është një himn poetik i frymëzuar, kushtuar Atdheut, natyrës dhe njerëzve të tij.

[Source](https://sq.wikibooks.org/wiki/Bag%C3%ABti_e_Bujq%C3%ABsi)
