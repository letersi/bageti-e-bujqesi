---
chapter_number: 1
title: Bagëti e Bujqësi
---

1. O malet' e Shqipërisë e ju o lisat' e gjatë!
1. Fushat e gjëra me lule, q'u kam ndër mënt dit' e natë!
1. Ju bregore bukuroshe e ju lumenjt' e kulluar!
1. Çuka, kodra, brinja, gërxhe dhe pylle të gjelbëruar!
1. Do të këndonj bagëtinë që mbani ju e ushqeni,
1. O vendëthit e bekuar, ju mëndjen ma dëfreni.
1. 
1. Ti Shqipëri, më ep nderë, më ep emrin shqipëtar,
1. Zëmrën ti ma gatove plot me dëshirë dhe me zjarr.
1. 
1. Shqipëri, o mëma ime, ndonëse jam i mërguar,
1. Dashurinë tënde kurrë zemëra s'e ka harruar.
1. 
1. Kur dëgjon zëthin e s'ëmës qysh e le qengji kopenë,
1. Blegërin dy a tri herë edhe ikën e merr dhenë,
1. Edhe në i prefshin udhën njëzet a tridhjetë vetë,
1. E ta trëmbin, ajy s'kthehet, po shkon në mes si shigjetë,
1. Ashtu dhe zëmëra ime më le këtu tek jam mua,
1. Vjen me vrap e me dëshirë aty nër viset e tua.
1. Tek buron ujët e ftohtë edhe fryn veriu në verë,
1. Tek mbin lulja me gas shumë dhe me bukuri e m'erë,
1. Ku i fryn bariu xhurasë, tek kullosin bagëtija,
1. Ku mërzen cjapi me zile, atje i kam ment e mija.
1. Atje lint diell' i qeshur edhe hëna e gëzuar,
1. Fat' i bardh' e mirësija në atë vënt janë mbluar;
1. Nat'atje'shtë tjatrë natë edhe dita tjatër ditë,
1. Në pyjet' e gjelbëruar, atje rrinë perënditë.
1. 
1. Mendje! merr fushat e malet, jashtë, jashtë nga qyteti,
1. Nga brengat, nga thashethemet, nga rrëmuja, nga rrëmeti.
1. 
1. Tek këndon thëllëza me gas edhe zogu me dëshirë,
1. E qyqja duke qeshur, bilbili me ëmbëlsirë,
1. Tek hapetë trëndafili, atje ma ka ënda të jem,
1. Bashkë me shpest edhe unë t'ia thërres këngës e t'ia them;
1. Të shoh kedhërit' e shqerrat, deshtë, cjeptë, dhëntë, dhitë,
1. Qiellin' e sbukuruar, dhenë me lul'e me dritë.
1. 
1. Vashë bukurosh'e bariut! që vjen me llërë përveshur,
1. Me zemërë të dëfryer e me buzëzë të qeshur,
1. Me dy shqerëza ndër duar, të bukura si dhe vetë,
1. Në sythit tënt e shoh gazë, që s'e kam gjetur ndë jetë.
1. Dashi sysk e me këmborë, q'e ke manar, po të vjen pas,
1. Dhe qeni me bes' i larmë të ndjek me dëshir' e me gas.
1. Dashç Perëndinë, pa më thua, a mos na pe bagëtinë?
1. Pash' atje pas më të gdhirë,... ja atje përtej tek vinë!
1. 
1. O! sa bukuri ka tufa! Sa gas bije bagëtija!
1. Vinë posi mblet' e plotë! I bekoftë Perëndija!
1. Nëpër shesh' e nër bregore janë përhapurë shqerrat,
1. E kecërit nëpër rripat dhe në gjethet e në ferrat;
1. Sa me vrap e me gas bredhin edhe lozin shok me shok,
1. Aty përhapenë me nxit aty mblidhenë prapë tok,
1. Edhe prapë tufë-tufë përhapenë duke bredhur,
1. Duke ikur me vrap shumë, duke lojtur, duke hedhur.
1. Nxitojn' e s'lodhenë kurrë edhe, kur i merr urija,
1. Secili futet në tufë, suletë ne mëm' e tija,
1. Posa gjen mëmën e dashur edhe me vrap i hyn në gji,
1. Rri më gjunjë dhe zë sisën e qumështin e ëmbël pi;
1. Pa e ëma me mall shumë, ndo dhi qoftë a ndo dele,
1. Bir' e vetëm e merr në gji me gas e me përkëdhele.
1. 
1. Sa të mirazë ke dhënë, Zot i math e i vërtetë!
1. E ç'nom të bekuar vure për çdo gjë q'është në jetë!
1. 
1. Sa më pëlqen blegërima, zër'i ëmbël' i bagëtisë,
1. Qëngji edhe kec'i bukur, që rri më gjunj' e pi sisë!
1. Përhapurë bagëtija nëpër sheshe, nëpër brinja,
1. Nër lajthi e nëpër dushnja, ndër murriza, në dëllinja;
1. Bijen zilet' e këmborët e fyelli e xhuraja,
1. Dheu bleron e gjelbërojnë fusha, male, brigje, maja,
1. Edhe gjithë gjë e gjallë ndjen në zemër një dëshirë,
1. Një gas t'ëmbël' e të shumë, o! sa bukur e sa mirë!
1. Pelën e ndjek mëz'i bukur, lopës i vete viçi pas,
1. Dellëndyshja punëtore bën folenë me të math gas,
1. Ogiçi ikën përpara, i bije tufës në ballë,
1. Me zemër të çelur shumë vete si trimi me pallë,
1. Zoqtë zënë këng' e valle dhe po kërcejn' e këndojnë,
1. E nëpër dega me lule si ëngjëllit fluturojnë,
1. Larashi ngrihet përpjetë, thua q'i shpie Perëndisë
1. Një lëvdatë të bekuar për gëzimt të gjithësisë,
1. 
1. Qielli sa ësht' i kthiellt e sa është sbukuruar!
1. E dielli sa ndrin bukur mbi lulet të lulëzuar!
1. Gjithë këto lule ç'janë, që u ngjallë menjëherë?
1. Ngaha qielli ke xbritur? Ver', o e bukura verë!
1. Çdo lulezë ka me vehte një emër e një fytyrë,
1. Një bukuri, një mirësi, një shtat, nj'erë e një ngjyrë,
1. Si dhe çdo dru e çdo pemë, edhe çdo bar e çdo fletë;
1. Sa është e bukur faq' e dheut! S'të zë syri gjë të metë.
1. 
1. Gjithë kjo bukuri vallë nga dheu të ketë mbleruar,
1. A me të matht të ti' Zoti pej parajs'e ka dërguar?
1. 
1. Veç një njeri shoh pa punë dhe të mjer' e të brengosur,
1. Të këputur, të mjeruar, të grisur e të rreckosur;
1. Lipën i gjori pa shpresë, se atje e pru përtimi,
1. S'i ka mbetur gas në zemrë, se s'i la vënt idhërimi.
1. Eshtë njeri, si dhe neve, po epini, o të pasur,
1. E mos e lini të urët dhe të mjer' e buzëplasur,
1. Se përtimn' e zi, q'e pruri të gjorën më këtë ditë,
1. Nuk' e dimë vet' e zgjodhi, apo ia dhanë Perënditë.
1. Edhe për një mizë, kur heq, i vjen keq njeriut të mirë,
1. Zëmëra s'thuhetë zëmrë me mos pasurë mëshirë.
1. 
1. Ah! edh' atje tej mbi udhë i duket i shkreti varri,
1. Rrethuar me lul'e me bar, një të gjori udhëtari,
1. Që ka vdekur i ri shumë e ka rarë lark shtëpisë,
1. Mërguar nga mëm' e motrë dhe pej gjithë njerëzisë;
1. Një zok i helmuar mi varrt i rri si mëmëzë dh'e qan,
1. Ndarë nga të gjithë shokët edhe zi për të mjerë mban.
1. 
1. Tomor! o mal i bekuar, fron i lartë, që rrij Zoti,
1. Pas fesë vjetrë që kishinë shqipëtarëtë qëmoti,
1. Dhe ti Mali-Plak i lartë, që me syt' e tu ke parë
1. Luftëra të mëdha shumë e punë që kanë ngjarë.
1. O malet' e Shqipërisë, që mbani kryet përpjetë,
1. Tëmerr e frikë përhapni, përpini qiejt e retë!
1. Të patundurë përjetë jini, pa, kur oshëtini,
1. Udhëtarit në zemër frikë të madhe i vini;
1. Keni shkëmbënj, gërxhe, lisa, lumënj dhe dëborë ndë gji,
1. Përsiprë lulez' e gjethe dhe brënda ergjënt e flori,
1. E ju fusha bukuroshe edhe të majm'e pëllore,
1. Ju sheshet e lulëzuar, ju bregore gjelbërore,
1. Q'u fali Zoti të mira, u mba me shumë pekule,
1. U dha bar e gjeth e veri, zoq e flutura e lule,
1. Zemërn' e varfërë time aty ndër ju e kam mbluar,
1. Tek buron nga gjithi juaj uj'i ftoht' e i kulluar;
1. Jam lark jush i dëshëruar edhe s'e duronj dot mallë,
1. Po s'e di si dua unë do t'u shoh një herë vallë?
1. 
1. Të paskësha vrapn' e veriut, të kisha krahë pëllumbi,
1. Nxitimn' e lumit me valë, q'ikën me vërtik si plumbi,
1. E të vija në gjit tuaj, nj' ujë të ftohtë të pinja,
1. Edhe nëpër ato hije një copë herë të rrinja,
1. Syt' e ballit t'i xbavitnja, zëmërënë ta dëfrenja,
1. Gazë, që paçë njëherë, prap' aty ndër ju ta gjenja.
1. Opopo! Kshu pse më vini përpara syve pa pushim,
1. O ditët' e djalërisë, o moj kohëz' e të rit tim?
1. 
1. O flutura krahëshkruar, që fluturon nëpër erë,
1. As merr dhe zëmrënë time me vehtezë dhe ma shpjerë
1. Nër malet të Shqipërisë, tek kullosën bagëtija,
1. Tek i fryn bariu xhurasë, tek më rrinë mënt' e mija,
1. Ku shkon me zile të madhe ogiçi përmes lajthisë,
1. Pa zjen e oshëtin mali ngaha zër'i bagëtisë;
1. Marrënë vrapn' e nxitojnë, derdhen në gjollë për kripë,
1. Dhëntë ndër shesh'e ndër brigje, dhitë në shkëmb e në rripë.
1. 
1. Bariu plak krabën në dorë edhe urdhëron të rinjtë,
1. E ata gjithë punojnë, ngriturë më bres përqinjtë;
1. Ca bëjnë vathën e shtrungën, ca ngrehin tëndën e stanë,
1. Kush sjell gjeth e karthj' e shkarpa, sicilido ndih më nj'anë;
1. Kush përvjel, kush qeth sheleknë, kush mjel dhitë, kush mjel dhëntë,
1. Njëri merr ushqen këlyshnë, jatëri përgëzon qëntë.
1. Stopani, bër'i zi sterrë, shikon bulmetn' e bekuar,
1. Tunt, bën gjalpë, djathë, gjizë edhe punon pa përtuar;
1. Udhëtar' e gjahëtorë, q'u bije udha ndër male,
1. U ep mish, qumësht, kos, dhallë, ajkë, djathë, bukëvale...
1. Kec'i mbeturë pa mëmë dhe i varfër' e i shkretë
1. Mënt mëmënë, që ka mbetur pa bir e pa gas në jetë.
1. Dëgjohet nga mez'i pyllit krism' e sëpatës s'druvarit,
1. E sharrësë që bën lëndë, edhe fyell'i shterparit.
1. 
1. Shterpari s'i qaset stanit, po nër pyje bij'e ngrihet,
1. Nëpër maja, nër bregore, rri, këndon a gdhënt, a shtrihet;
1. S'i trembetë syri kurrë, vetëm ajy dit' e natë,
1. Nga ujku e nga kusari s'ka frik', as nga lis'i gjatë,
1. As nga shkëmbënjt' e nga pylli, as gogolëtë s'e hanë,
1. Armëtë ka shok e vëlla, mëm' e motërë xhuranë;
1. Miqt' e ti shqeratë janë, kecërit, dhitë, dhëntë,
1. Cjeptë, ziletë, këmborët, deshtë e më tepër qëntë,
1. Që s'flenë, po rrin' e ruajn bagëtinë dhe barinë,
1. Kur e shohin, tundin bishtin dhe me gas të math i vinë;
1. S'e hanë njerin' e mirë edhe mikun' e udhëtarë,
1. Se i njohën; po të liknë, egërsirënë, kusarë.
1. Vjen nata, e lë në t'errët, del hëna, i përhap dritën,
1. Vjen mëngjesi, sbardhëllehet, lint' dielli, i bije ditën.
1. Yjtë, hëna, dielli, shënja, lindin e prapë perëndojnë,
1. Gjithë ç'lëvrijnë nër qiej, përpara syvet i shkojnë.
1. Mblidhen ret' e hapësira bënetë e zezë sterrë,
1. Vetëtimat e gjëmimet nisin e shiu zë të bjerë;
1. Bariu vë gunën në kokë, z'eshkën me herët të parë,
1. Ndes shkarpat sakaqëherë, e lisnë fyl, dhe bën zjarrë;
1. Fishëllen e thërret qentë sicilin me emër veçan,
1. Pa, kur derdhetë Baliku, ujkun' e zë edh'e përlan,
1. Se bisha, që bije dëmnë, errësir' e mjergull kërkon,
1. Papo bariu shum' ahere vë re dhe mba vesh e dëgjon,
1. Dhe sokëllin me zë të madh, tunden malet e shkëmbenjtë,
1. Gumzhitin pyjet' e veshur e oshëtijnë përrenjtë!
1. 
1. Esht' e lehtë dhi e stanit, që kullot gjethen e malit,
1. Dhe bij'e fle majë shkëmbit e pi ujëthit e zallit;
1. Dhi e shtëpis' ësht' e plokshtë, fle në vath' e nënë strehë
1. E pi ujët e rrëkesë edhe shtrihetë në plehë;
1. Esht' e butëz' edh'e qetë dhe e urtë si manare,
1. Nuk' është si malësorja, andaj i thonë bravare.
1. 
1. Në pshat, posa sbardhëllehet, sheh një plakëzë të gjorë,
1. Ngrihet, hap derën ngadale, e del me kusi në dorë,
1. Rri në derëzët të shtrungës, dhe djali duke dremitur
1. I nget bagëtin' e delen, i mjel plakëz' e drobitur.
1. Plaku lë shkopnë mënjanë e bën gardhin a zë shteknë,
1. Bariu vë tufën përpara, vasha përkëdhel sheleknë,
1. Nusja pshi e ndreq shtëpinë edhe bën bukën e gjellën,
1. I shoqi sheh kanë, lopën, viçnë, demnë, kalën, pelën,
1. Mushkën, që ësht' e harbuar edhe bashkë me gomarë
1. Rrahënë të hedhin murë, të hanë bimën a barë.
1. Një grua vete në krua, e jatëra zë të tuntnjë,
1. Një sheh pulat, miskat, rosat, dhe tjatëra bën çtë muntnjë.
1. 
1. Na hyjnë shumë në punë kafshëtë dhe bagëtija,
1. Na i dha në këtë jetë shok' e ndihmës Perëndija.
1. Të mos ishte gjë e gjallë, njeriu s'rronte dot në jetë,
1. Do të vdiste nga uria, do t'ish lakuriq e shkretë;
1. Gjë e gjallë na vesh, na mbath dhe na ushqen e na xbavit,
1. Kur shtohet e vete mbarë; jetënë tën' e përsërit.
1. Edhe dheu, që na ep drithë, sido ta kemi punuar,
1. Nukë pjell mirë si duam, po s'e patmë plehëruar.
1. O shokëtë e njeriut, Zoti u shtoftë e u bekoftë!
1. Dhe shpirti im mik përjetë, sindëkur ka qën' u qoftë.
1. Kafshët, edhe bagëtinë, që u ka kaqë nevojë,
1. Njeriu duhetë t'i shohë, t'i ketë kujdes, t'i dojë.
1. Të mos t'i mundojmë kurrë, po si fëmijë t'i kemi,
1. Eshtë mëkat edhe fjalë të ligë për to të themi.
1. 
1. Dellëndyshe bukuroshe, që thua mijëra fjalë,
1. Dhe të k'ënda vahn' e lumën, që vjen me vrap e me valë,
1. A mos vjen nga Shqipëria? Eni vjen pej Çamërie
1. Me këto milëra fjalë e me gluhë perëndie?
1. Apo vjen nga Labëria, pra më duke kaqë trime,
1. Edhe fjalëtë që thua më gëzojnë zëmrën time,
1. Q'është thier, bërë posi një pasqirë,
1. Duke këputur nga cmagu, që s'e kanë vartur mirë,
1. Apo vjen nga fush'e Korçës, nga vënd'i mir' e i gjerë,
1. Pej zembrësë Shqipërisë, që del gjithë bot' e ndjerë?
1. A më vjen pej Malësie, pej Skrapari, pej Dobreje,
1. Nga Vijosa, nga Devolli, pej Vlor' e pej Myzeqeje?
1. 
1. Të munjam të fluturonja e të kishnjam krahë si ti,
1. Me gas të math do t'i vinjam Shqipërisë brënda në gji!
1. Për me marrë drejt Shkumbinë edh' Elbasan' e Tiranën,
1. E me ardh ke ti, o Shkodrë, të shof Drinin e Bujanën,
1. Kostur, Përlep, Fëllërinë, Dibrë, Ipek e Jakovën,
1. Mat' e Ysqyp e Prështinë dhe Mirëdit' e Tetovën;
1. Krojënë e Skënderbegut, q'i ka pas dhan ner Shqypnisë,
1. Tue bam me trimni luftë, e m'e munt mren e Tyrqisë.
1. 
1. Durres, o qytet i bukur, që je kërthiz' e mëmëdheut!
1. Edhe ti Leshi me emrë, që ke eshtrat e Skënderbeut!
1. Burrat tuaj aqë trima do ta lenë vall' Ylqinë
1. Edhe gjithë shqipëtarët ta mbanjë armiku ynë?
1. Nukë më ngjan e s'e besonj, kam te zoti shumë shpresë,
1. Shqipëria këtej-tutje kshu po nukë do të mbesë.
1. 
1. Dua të dal majë malit, të shoh gjithë Arbërinë,
1. Vëllezërit shqipëtarë, që venë në pun' e vinë,
1. Burrat trima me besë dhe shpirtmir' e punëtorë,
1. Dhe fushatë gjithë lule e malet me dëborë.
1. 
1. O fushazëtë pëllore, që m'ushqeni Shqipërinë,
1. Do të këndoj bukurinë tuaj edhe bujqësinë.
1. 
1. Ti perndi e ligjërisë, që rri në malt të Tomorit,
1. Unju posht' e më ndih pakë, o motra im'e të gjorit!
1. Më ke leshrat të florinjta e të ergjëndtë krahrorë,
1. Ball' e gush' e faq'e llërë dhe këmb' e duar dëborë;
1. Sikundër do malësorët dhe pyjet e bagëtinë,
1. Duaj edhe fusharakët dhe arat' e bujqësinë,
1. 
1. Edhe ti, o mëmëz' e dheut, q'i fale dheut aq' uratë,
1. Sa pjell mijëra të mira e kurrë s'mbetetë thatë,
1. I dhe lul'e bar e gjethe, bim' e drith' e pem' e drurë,
1. Mlodhe gjithë bukuritë edhe kanisk ia ke prurë.
1. 
1. Të keqen, o symëshqerë, shikomë një herë në syt!
1. Si lulet' e si bilbili edhe unë jam djali yt.
1. 
1. Gjithë këto farë lulesh e këtë të bukur erë,
1. Këtë mblerim, këto gjyrë vallë nga ç'vent'i kesh nxjerrë!
1. O sa e madhe bukuri! As më thua ku e more!
1. O bukuroshe, t'u bëfsha, ngaha gjiri yt e nxore?
1. Apo me dorët të bukur e more nga gjir'i Zotit,
1. Nga qielli, nga parajsa, nga prehër' e plotë i motit?
1. Kudo shkel këmbëza jote, gëzohet vendi e mbleron,
1. Tekdo heth sythit e qeshur, bukuri' atje lulëzon!
1. 
1. Ti zbukuron faqen' e dheut, ti do e ushqen njerinë,
1. Më të gjallë, dhe pas vdekjes e pret duke hapur gjinë!
1. Vjen dimëri, t'i than lulet, ti me një frym' i ngjall prapë,
1. Napënë q'u heth përsiprë, ua heq me ver' e me vapë.
1. 
1. Bujkun e xgjuan me natë edhe vë përpara qetë,
1. Nisetë pa zbardhëllyer për punëzët të vërtetë;
1. Mer pluarin e parmëndën, zgjedhën, tevliknë, hostenë,
1. Kafshën, farën, shoknë, bukën, trajstënë, lakrorë, qenë...
1. Shërbëtor'i mëmës' së dheut, q'e ka zëmrënë plot shpresë,
1. Del kur hapet trëndafili dhe bari 'shtë gjithë vesë;
1. I falet Zotit t'vërtetë dhe zihet nga pun' e mbarë,
1. Zëmërzën e ka të bardhë dhe të qruar e të larë.
1. 
1. Pa lodhur e pa këputur, pa djersë e pa mundime,
1. Njeriu i gjorë në jetë nukë gjen dot as thërrime,
1. Si të punosh dit' e natë e të bësh ç'duhenë gjithë,
1. Ahere kërko nga Zoti të t'apë bukëz' e drithë.
1. 
1. Njeri, puno, mos psho kurrë dhe lark nga makutërija,
1. Zëmërnë kije të gjerë, mos ki keq, pa t'ep Perndija.
1. 
1. Puna ka duk e uratë, Zot'i math e ka bekuar,
1. Njerinë mi faqet të dheut e dërgoi për të punuar.
1. 
1. Ver' o e bukura verë, që na vjen nga i madhi Zot
1. Me mirësi, me bukuri, me gas të math, me duar plot,
1. Sindëkur çel trëndafilë, e i fal bilbilit zënë,
1. Ashtu na bije nga qielli një gas në zëmërt tënë.
1. 
1. Zot'i e i vërtetë për të ushqyer njerinë,
1. Për të zbukuruar dhenë, për të shtuar mirësinë,
1. I dha zjarr e flakë diellit, i fali dhe shinë resë,
1. Bëri dimërin e verën dhe zemrës san' i dha shpresë.
1. 
1. Për të arriturë rrushnë ç'ka punuar Perëndija,
1. Qielli, dheu, dielli, shiu, njeriu, tërë gjithësija!
1. S'është çudi pse na dëfren ver' e bukur zemrën tënë;
1. Ç'ka punuar Perëndija edhe njeriu, sa e bënë!
1. Ju shokë, kur pini verën, mos dehi, mos zëmërohi,
1. Mos u zihni, mos u shani, mos lëvdohi, mos qërtohi,
1. Se përçmoni Perëndinë, q'i ka falur hardhisë rrush,
1. Edhe kërkon dashurinë e ndodhet pshetazi ndaj jush;
1. Po gëzohi, prehi, qeshni, duhi, xbaviti, dëfreni,
1. Flisni fjalë të pëlqyer, loni, këndoni, kërceni,
1. Bëjeni zëmrën të gjerë edhe shtoni dashurinë,
1. Mirësinë, njerëzinë dhe besën e miqësinë,
1. Se në breng' e në të keqe, në punë e në të pirë,
1. Mirretë vesh njeriu i lik, njihetë njeriu i mirë.
1. 
1. A e shihni gjithësinë, yjtë, Diellinë, Hënën,
1. Dhenë, erën, retë, kohën, Kashtën' e Kumtërit, Shënjën,
1. Si janë përveshur gjithë edhe lëçijn' e punojnë,
1. Njëri-tjatërit i ndihin, ashtu punën e mbarojnë.
1. Në mest të këti rrëmeti, të punëtorëve shumë,
1. Njeriu duhet të lëçinjë, apo të bjerë në gjumë?
1. 
1. Mundohetë punëtori, po në zemërzët të qetë
1. Sa gas të math ndjen, kur njëra që hoth, i pjell dymbëdhjetë!
1. Kur e sheh kallin' e plotë të kërrusurë nga barra,
1. Dhe parajsën e vërtetë të tfaqurë nëpër ara,
1. Kur heth lëmën e mbleth toknë, ndan bykn' e kashtën mënjanë,
1. U heth kuajve e qevet, që janë lodhur, të hanë,
1. Kur e përmbush plot shtëpinë me drith' e me gjë të gjallë,
1. Shtrohet me uri në bukë e ha me djersë në ballë.
1. 
1. Sheh pjergullnë, manë, fiknë, thanënë, arrën, ullinë,
1. Mollën, dardhën, pjeshkën, shegën, vadhënë, ftuan, qershinë,
1. Kumbullatë, zerdelinë, ngarkuar me pemë gjithë,
1. Oborrë plot gjë të gjallë, shtëpinë mbushur me drithë,
1. Dhe zëmëra i gëzohet, pa i faletë Perëndisë,
1. Q'e çpërblen punën e djersën e mundimn' e njerëzisë.
1. 
1. Qysh rroit mblet' e uruar dhe ven' e vin e lëçijnë,
1. Ca huallinë ndërtojnë, ca nëpër lule shëndijnë.
1. O ç'punë me mënt punojnë, sa bukur e bëjn' e mirë!
1. N'apin dyllëtë, q'ep dritë, dhe mjaltë fjesht' ëmbëlsirë.
1. Dhe punëtorët' e mirë m'atë mëndyrë punojnë,
1. Edhe gjithë njerëzija me mundimt t'atyre shkojnë;
1. Njëri mih, jatëri lëron, njëri mbjell, jatëri prashit,
1. Kush t'harr, kush korr, kush mbledh duaj, kush shin, kush sharton, kush krasit,
1. Një bën pluar' e sëpatën, një parmendën, një shtëpinë,
1. Një pret e qep, një merr e ep, një mbath, një shikon mullinë,
1. Çdo njeri një farë pune bën në mest të shoqërisë,
1. Kjo ësht' udh' e Perëndisë, ky ë nom i gjithësisë.
1. Edh' ajo miza përdhese, ç'i duhetë për të ngrënë,
1. Eshtë rrahur e përpjekur e me kohëz' e ka vënë.
1. Ka një punë të punonjë si çdo gjë q'është në jetë,
1. Kshu e ka thënë me kohë Zot'i math e i vërtetë.
1. 
1. Bujku mundohet në verë, po në dimër rri e prëhet,
1. Sheh shtëpizënë më kamje, edhe zëmëra i bëhet,
1. Gratë të gjitha punojnë n'avlëmënt e në të tjera,
1. Edhe jashtë fryn e bije, por kur na trokëllin dera:
1. Eshtë nj'udhëtar i gjorë, që ka mbetur në dëborë,
1. I kanë ngrirë të mjerit vesh' e goj, e këmb' e dorë;
1. Ngrihet i zot'i shtëpisë edhe të huajthin e merr,
1. E vë në kryet të vatrës me njerëzi, me të math nder,
1. Posa e shohënë që vjen, i ngrihen gjithë fëmija,
1. Se të huajnë më derë na e dërgon Perëndia,
1. Pa i bëjnë zjarr e ngrohet edh'e mbajnë me të mirë,
1. I sjellin shtresë të flerë edhe të ngrën' e të pirë.
1. 
1. Kështu të huajt' e miqtë njeriu q'është i uruar
1. I pret me krahëror hapur e i përcjell të gëzuar.
1. 
1. Në verë që çelen lulet, qielli ndrin si pasqyrë,
1. Sbukurohetë faq'e dheut e merr mijëra fytyrë;
1. Pa ngjallenë më çdo lule, më çdo bar e më çdo fletë
1. Gjëra të gjalla me mijë, rroitin nga dheu si mbletë.
1. 
1. Shpest' e mizatë këndojnë e kuajtë hingëllijnë,
1. Lulet' e bukura m'erë si ar e si flori ndrijnë,
1. Bujku nget pëndën e lëron, mbjell a bën gati ugarë,
1. Kalorësi i shkon njatë dhe i thotë -- puna mbarë --
1. Papo merr anën e lumit me zëmërë të gëzuar,
1. Këndon, fishëllen e vete ngadalë, duke mejtuar;
1. Vë re lumën e kulluar, që ikën me ligjërime,
1. E ndër ment të ti i bije ca t'ëmbla shumë mejtime.
1. Vashazëtë bukuroshe, posi shqerratë manare,
1. Si kapërollet e malit, si thëllëzatë mitare,
1. Venë të lajnë në lumë gjithë tok duke kënduar,
1. Me gas në sy e në buzë e me lulezë nër duar;
1. Përveshin llërët' e bardha dhe të majm'e të perndijta,
1. Pulpazëtë bukuroshe e këmbëzët' e kërthijta.
1. Dellëndyshja që fluturon e ndehetë përmi lumë,
1. U afrohetë si mike e u thotë fjalë shumë,
1. Dhe mëshqer' e përkëdhelur vjen në lumë të pij' ujë,
1. A të prëhetë në hije, a të bënjë gjë rrëmujë.
1. 
1. Bari, bima vatur më bres e bujku shum' i gëzuar,
1. Si bariu kur merr kërthinë edh'e përkëdhel ndër duar.
1. 
1. Bilbili ia thotë bukur, lumi vete gjithë valë,
1. Ep erën e Perëndisë trëndafili palë-palë.
1. 
1. Veç një vashëz' e mjerë qan të motrënë, q'e ka lënë,
1. O! është mbuluar në dhe vashëza fytyrëhënë!
1. Mëma dhe motëra mbetur në zi e në vaj të shumë,
1. Dhe shqerra manarez' e saj, e përzieshmez' e për lumë!
1. Të këput shpirtinë plaka, kur zë dhe nëmëron e qan,
1. Ah, i ziu njeri në jetë sa heq e sa duron e mban!
1. 
1. Vashën vërtet e mbuluan, po shpirt'i saj në qiej shkoi,
1. Hapi krahëthit e lehtë, në hapësirat fluturoi;
1. Bukuri e saj u përzje me bukurizët të prillit,
1. Me fjalëzët të bilbilit, me erët të trëndafilit,
1. Gjësendi s'humbetë kurrë e gjë s'vdes me të vërtetë,
1. Mase ndryshohenë pakë, po janë në këtë jetë;
1. As shtohet, as pakësohet, as prishetë gjithësija,
1. Vdesën e ngjallenë prapë si gjith' edhe njerëzija.
1. Këtu janë gjithë ç'janë e gjithë ç'gjë munt të jetë,
1. Engjëllitë, Perënditë dhe ajy Zot'i vërtetë!
1. Se një trup e një shpirt është gjithësia, që s'ka anë,
1. Të gjallë edhe të vdekur gjithë brënda në të janë.
1. 
1. Perëndija njerin' e parë e mori prej dore vetë,
1. E zbriti mi faqet e dheut, q'ish me lulez' e fletë,
1. Më të drejtënë të themi, mbi faqet të dheut e ngriti,
1. E bëri të zotthin' e dheut edhe kështu e porsiti:
1. 
1. Nga kjo baltë të kam bërë, rri këtu, më paç uratë,
1. Mos u loth e mos psho kurrë, po përpiqu dit' e natë,
1. Sheh si punon gjithësija? Ashtu të punosh edhe ti,
1. Të mos rrish kurrë pa punë e të vësh duartë në gji.
1. Mos u bëj i lik e i keq, i paudh' e i pabesë,
1. I rrem, i ndyrë, i dëmshim, i rënduar e pa shpresë,
1. Mërgohu nga të këqijat, pej çdo farë ligësije,
1. Pej nakari, pej lakmimi, pej vjedhjeje, pej marrëzije,
1. Mos vra, mos merr tek s'ke vënë, edhe ki nom dashurinë,
1. Bes' e fe ki urtësinë, të drejtënë, mirësinë.
1. Në bëfsh mirë, liksht s'gjen kurrë, po, në bëfsh liksht, mos prit mirë,
1. Ki dëshirë për të mirë dhe në zemërë mëshirë,
1. Ji i but', i urt', i vyer e mos u bëj kurrë makut,
1. I egër e i mërzitur dh'i mahnitur si madut,
1. Mos ju afro dhelpërisë, po së drejtësë iu nis pas;
1. Në dëgjofsh fjalët' e mija, do të jesh gjithënjë në gas.
1. 
1. Nga gjithë ç'pat gjithësia, të kam dhënë dhe ty pjesë,
1. Në u bëfsh si them, i mirë, emr'i math do të të mbesë.
1. Të kam dhënë mënt të mësosh, të vërtetën me të ta shohç,
1. Dhe zëmër' e vetëdijë, të mir' e të drejtën ta njohç,
1. Do të të lë dhe nevojën, udhën të të tregonjë,
1. Të të ndihnjë më çdo punë, të të psonj' e të të zgjonjë.
1. Gjithë të mirat që janë, këtu në dhet i kam mbuluar,
1. Po gjësendi në shesh s'nxjerr dot pa dirsur e pa munduar;
1. I gjen të gjitha me kohë, po rrëmo thell' e më thellë,
1. C'do gjë që të duhet, kërkoje, barku i ti do ta pjellë.
1. Sa gjërërazë të vlera do të gjesh ti këtu brenda,
1. Edhe përsipërë soje, e sa do të t'i ket ënda!
1. 
1. Me fuqit që të kam dhënë, them që të vinjë një ditë
1. Të marrç udhën e së mirës e të gjesh të madhe dritë,
1. Të marrç vesh dalengadalë sa punëra që kam bërë,
1. Diell, hënë, yj, dhe, qiej e gjithësinë të tërë!
1. 
1. Po që u bëre i urtë, mua më ke afër teje,
1. Ndryshe, qofsh i mallëkuar edhe mërguar prej meje!
1. 
1. Të parit tënë perndia këto fjalë vetëm i tha,
1. I fali gjithë të mirat, i dha uratën dhe e la.
1. Det i p'an'i mirësisë, q'emrin tënd s'e zë dot ngoje,
1. Qysh e ngrehe gjithësinë pa lënë farë nevoje!
1. Fali njeriut urtësinë, mirësinë, njerëzinë,
1. Butësinë, miqësinë, dashuri, vëllazërinë;
1. Epu sheshevet lul' e bar dhe pyjevet gjeth e fletë,
1. Resë shi, aravet bimë e mos lerë gjë të metë,
1. Fali erë trëndelinës, manushaqes, trëndafilit,
1. Kalliut bukë, mizës pjesë, zogut ngrënie, zë bilbilit,
1. E drurëvet epu pemë dhe uratë bagëtisë,
1. Dërgo dhëmbj' e kujdes për to në zëmërt të njerëzisë;
1. Epi pjergulls' e vështit rrush dhe vozësë fali verë,
1. Mos e lerë pa të kurrë, kurrë thatë mos e lerë;
1. Fali diellit flak e zjarr dhe hënës e yjet dritë,
1. Edhe detit uj' e kripë, gjithësisë jet e ditë.
1. Yjtë le të vinë rrotull dhe njerëzit të punojnë,
1. Të dëfrejn' e të gëzohen dhe si vëllezër të shkojnë.
1. 
1. Tregomu dhe shqipëtaret udhën e punës së mbarë,
1. Bashkomi, bëmi vëllezër edhe fjeshtë shqipëtarë,
1. Falmi, falmi Shqipërisë ditën e bardh' e lirisë,
1. Udhën e vëllazërisë, vahn' e gjithë mirësisë.
1. 
1. Nxirr të vërtetën në shesht, paskëtaj të mbretëronjë,
1. Errësira të përndahet, gënjeshtëra të pushonjë.
